﻿using System;
using System.Linq;
using System.Web.Mvc;
using Choffmeister.Security;

namespace Choffmeister.Mvc.Security
{
    [AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public sealed class AuthParameterAttribute : ActionFilterAttribute
    {
        private readonly string _verb;

        public string Verb
        {
            get { return _verb; }
        }

        public string ParameterNames
        {
            get;
            set;
        }

        public AuthParameterAttribute(string verb)
        {
            _verb = verb;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            using (IAuthorizationHandler authorizationHandler = DependencyResolver.Current.GetService<IAuthorizationHandler>())
            {
                if (string.IsNullOrEmpty(this.ParameterNames))
                {
                    throw new Exception("Must pass at least one parameter name.");
                }

                string[] parameterNames = this.ParameterNames.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string parameterName in parameterNames)
                {
                    if (!filterContext.ActionParameters.ContainsKey(parameterName))
                    {
                        throw new Exception(string.Format("Invalid parameter name {0}.", parameterName));
                    }
                }

                if (parameterNames.Length == 0)
                {
                    throw new Exception("Must pass at least one parameter name.");
                }
                else if (parameterNames.Length == 1)
                {
                    object parameter = filterContext.ActionParameters[parameterNames.First()];

                    if (authorizationHandler.Authorize(_verb, parameter) != true)
                    {
                        filterContext.Result = new HttpUnauthorizedResult();
                    }
                }
                else
                {
                    object[] parameters = parameterNames.Select(n => filterContext.ActionParameters[n]).ToArray();

                    if (authorizationHandler.Authorize(_verb, parameters) != true)
                    {
                        filterContext.Result = new HttpUnauthorizedResult();
                    }
                }
            }
        }
    }
}