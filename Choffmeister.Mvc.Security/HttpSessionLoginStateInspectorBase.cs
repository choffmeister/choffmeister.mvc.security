﻿using System.Web;

namespace Choffmeister.Mvc.Security
{
    public abstract class HttpSessionLoginStateInspectorBase<TUserAccountType> : ILoginStateInspector<TUserAccountType>
        where TUserAccountType : class
    {
        private TUserAccountType _currentUser;

        public bool IsAuthenticated
        {
            get { return HttpContext.Current.Request.IsAuthenticated; }
        }

        public TUserAccountType AuthenticatedUserAccount
        {
            get
            {
                if (!this.IsAuthenticated)
                {
                    return null;
                }

                if (_currentUser == null)
                {
                    _currentUser = this.GetUser(HttpContext.Current.User.Identity.Name);
                }

                return _currentUser;
            }
        }

        protected abstract TUserAccountType GetUser(string username);
    }
}