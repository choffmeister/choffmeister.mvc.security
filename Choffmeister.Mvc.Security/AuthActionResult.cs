﻿using System;
using System.Web.Mvc;

namespace Choffmeister.Mvc.Security
{
    public sealed class AuthActionResult : ActionResult
    {
        private readonly object _resultToAuthorize;
        private readonly Action<object> _onGrantedAction;
        private readonly ActionResult _actionResult;

        public object ResultToAuthorize
        {
            get { return _resultToAuthorize; }
        }

        public Action<object> OnGrantedAction
        {
            get { return _onGrantedAction; }
        }

        public ActionResult ActionResult
        {
            get { return _actionResult; }
        }

        public AuthActionResult(object resultToAuthorize, ActionResult actionResult)
            : this(resultToAuthorize, null, actionResult)
        {
        }

        public AuthActionResult(object resultToAuthorize, Action<object> onGrantedAction, ActionResult actionResult)
        {
            if (actionResult == null)
                throw new ArgumentNullException("actionResult");
            if (actionResult is AuthActionResult)
                throw new ArgumentException(string.Format("Argument actionResult must not be of type '{0}'.", typeof(AuthActionResult).FullName));

            _resultToAuthorize = resultToAuthorize;
            _onGrantedAction = onGrantedAction;
            _actionResult = actionResult;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            _actionResult.ExecuteResult(context);
        }
    }
}