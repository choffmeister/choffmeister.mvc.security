﻿using Choffmeister.Security;

namespace Choffmeister.Mvc.Security
{
    public class IsAuthenticatedAuthorizationHandler : AuthorizationHandlerBase
    {
        private readonly ILoginStateInspector<object> _loginStateInspector;

        public IsAuthenticatedAuthorizationHandler(ILoginStateInspector<object> loginStateInspector)
            : base("authenticated")
        {
            _loginStateInspector = loginStateInspector;
        }

        public override bool? Authorize(string verb, object domain)
        {
            if (!_loginStateInspector.IsAuthenticated)
            {
                return false;
            }

            if (verb == null && domain == null)
            {
                return true;
            }

            return this.Successor != null ? this.Successor.Authorize(verb, domain) : null;
        }
    }
}