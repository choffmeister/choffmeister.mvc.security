﻿using System;
using System.Web.Mvc;
using Choffmeister.Security;

namespace Choffmeister.Mvc.Security
{
    [AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public sealed class AuthResultAttribute : ActionFilterAttribute
    {
        private readonly string _verb;

        public string Verb
        {
            get { return _verb; }
        }

        public AuthResultAttribute(string verb)
        {
            _verb = verb;
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            using (IAuthorizationHandler authorizationHandler = DependencyResolver.Current.GetService<IAuthorizationHandler>())
            {
                AuthActionResult authResult = filterContext.Result as AuthActionResult;

                if (authResult == null)
                {
                    throw new Exception(string.Format("Returned result of type {0} but {1} was expected.",
                        filterContext.Result.GetType().FullName, typeof(AuthActionResult).FullName));
                }

                if (authorizationHandler.Authorize(_verb, authResult.ResultToAuthorize) != true)
                {
                    filterContext.Result = new HttpUnauthorizedResult();
                }
                else if (authResult.OnGrantedAction != null)
                {
                    authResult.OnGrantedAction(authResult.ResultToAuthorize);
                }
            }
        }
    }
}