﻿namespace Choffmeister.Mvc.Security
{
    public interface ILoginStateChanger
    {
        void Login(string username, bool persistent);

        void Logout();
    }
}