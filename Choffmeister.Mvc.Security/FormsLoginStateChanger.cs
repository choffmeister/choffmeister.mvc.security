﻿using System.Web.Security;

namespace Choffmeister.Mvc.Security
{
    public class FormsLoginStateChanger : ILoginStateChanger
    {
        public void Login(string username, bool persistent)
        {
            FormsAuthentication.SetAuthCookie(username, persistent);
        }

        public void Logout()
        {
            FormsAuthentication.SignOut();
        }
    }
}