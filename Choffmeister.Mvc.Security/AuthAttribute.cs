﻿using System;
using System.Web.Mvc;
using Choffmeister.Security;

namespace Choffmeister.Mvc.Security
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public sealed class AuthAttribute : FilterAttribute, IAuthorizationFilter
    {
        private readonly string _verb;

        public string Verb
        {
            get { return _verb; }
        }

        public object Domain
        {
            get;
            set;
        }

        public AuthAttribute(string verb)
        {
            _verb = verb;
        }

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            using (IAuthorizationHandler authorizationHandler = DependencyResolver.Current.GetService<IAuthorizationHandler>())
            {
                if (authorizationHandler.Authorize(_verb, this.Domain) != true)
                {
                    filterContext.Result = new HttpUnauthorizedResult();
                }
            }
        }
    }
}