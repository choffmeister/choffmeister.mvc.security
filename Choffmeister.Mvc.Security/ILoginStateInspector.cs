﻿namespace Choffmeister.Mvc.Security
{
    public interface ILoginStateInspector
    {
        bool IsAuthenticated { get; }
    }

    public interface ILoginStateInspector<out TUserAccountType> : ILoginStateInspector
        where TUserAccountType : class
    {
        TUserAccountType AuthenticatedUserAccount { get; }
    }
}